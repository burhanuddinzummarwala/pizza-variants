package com.droid.pizza.variants.infra.di;

import com.droid.pizza.variants.PizzaVariantsApplication;
import com.droid.pizza.variants.home.HomeActivity;
import com.droid.pizza.variants.infra.viewmodel.ViewModelModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, ViewModelModule.class})
public interface AppComponent {
    void inject(PizzaVariantsApplication application);

    void inject(HomeActivity activity);
}
