package com.droid.pizza.variants.home;

import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.droid.pizza.variants.PizzaVariantsApplication;
import com.droid.pizza.variants.R;
import com.droid.pizza.variants.databinding.ActivityHomeBinding;
import com.droid.pizza.variants.infra.api.model.ExcludeList;
import com.droid.pizza.variants.infra.api.model.VariantGroup;
import com.droid.pizza.variants.infra.api.model.Variation;
import com.droid.pizza.variants.infra.interfaces.OnGenericRvItemClickListener;
import com.droid.pizza.variants.infra.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.droid.pizza.variants.infra.viewmodel.ApiStatus.HIDE_PROGRESS;
import static com.droid.pizza.variants.infra.viewmodel.ApiStatus.SHOW_PROGRESS;

public class HomeActivity extends AppCompatActivity implements OnGenericRvItemClickListener {
    private static final String TAG = "HomeActivity";
    @Inject
    protected HomeViewModel viewModel;
    private ActivityHomeBinding binding;
    private GroupsAdapter groupsAdapter;
    private List<VariantGroup> itemList = new ArrayList<>();
    private List<List<ExcludeList>> excludeList = new ArrayList<>();
    private ArrayList<VariantGroup> originalList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        PizzaVariantsApplication.getInstance().getAppComponent().inject(this);
        binding.setViewModel(viewModel);

        viewModel.apiCallToFetchVariants();
        initView();
        setUpMvvm();
    }

    private void initView() {
        groupsAdapter = new GroupsAdapter(itemList, this);
        binding.ahRvGroups.setAdapter(groupsAdapter);
    }

    private String originJson;

    private void setUpMvvm() {
        viewModel.snackbarMessage.observe(this, (Observer<Integer>) stringResId -> {
            Snackbar.make(binding.activityHomeContainer, stringResId, Snackbar.LENGTH_LONG).show();
        });
        viewModel.apiStatus.observe(this, status -> {
            if (SHOW_PROGRESS.equals(status)) {
                Utils.showProgress(this);
            } else if (HIDE_PROGRESS.equals(status)) {
                Utils.stopProgress();
            }
        });
        viewModel.variantGroupSingleLiveEvent.observe(this, groupsList -> {

            originalList = (ArrayList<VariantGroup>) groupsList.clone();
            originJson = new Gson().toJson(groupsList);
            Log.d("item", originJson);

            itemList.clear();
            itemList.addAll(new Gson().fromJson(originJson, new TypeToken<ArrayList<VariantGroup>>() {
            }.getType()));

            groupsAdapter.notifyData(itemList);
        });
        viewModel.excludeListSingleLiveEvent.observe(this, excludeList -> {
            this.excludeList = excludeList;
            Log.e(TAG, "setUpMvvm: " + excludeList.size());
        });
    }

    @Override
    public void onRvItemClick(int parentPosition, int childPosition, Variation variationObj) {
        List<VariantGroup> lastSaveList = (new Gson().fromJson(originJson, new TypeToken<ArrayList<VariantGroup>>() {
        }.getType()));
        for (int i = 0; i < itemList.size(); i++) {

            for (int j = 0; j < itemList.get(i).getVariations().size(); j++) {
                if (i != parentPosition) {
                    lastSaveList.get(i).getVariations().get(j).setSeleceted(itemList.get(i).getVariations().get(j).isSeleceted());
                }
            }

        }
        itemList.clear();
        itemList.addAll(lastSaveList);

        Log.e(TAG, "onRvItemClick: groupId: " + itemList.get(parentPosition).getGroup_id()
                + ", variationId: " + variationObj.getId()
                + ", name: " + variationObj.getName()
                + ", isSelected: " + variationObj.isSeleceted());

        ExcludeList excludeItem = new ExcludeList();
        excludeItem.setGroupId(itemList.get(parentPosition).getGroup_id());
        excludeItem.setVariationId(variationObj.getId());

        ArrayList<Integer> matchedPosition = new ArrayList<>();
        for (int i = 0; i < excludeList.size(); i++) {  // runs 2 times
            for (ExcludeList inner : excludeList.get(i)) {
                if (inner.getGroupId().equals(excludeItem.getGroupId())
                        && inner.getVariationId().equals(excludeItem.getVariationId())) {
                    matchedPosition.add(i);
                    break;
                }
            }
        }

        // total number of exclude variations available
        if (matchedPosition.size() > 0) {
            // perform operations on new Thread
            new Thread() {
                @Override
                public void run() {
                    for (Integer mPosition : matchedPosition) {
                        Log.e(TAG, "matchedPositions: " + excludeList.get(mPosition).size());

                        for (ExcludeList inner : excludeList.get(mPosition)) {

                            // ignore current variation
                            if (!inner.getGroupId().equals(excludeItem.getGroupId())
                                    && !inner.getVariationId().equals(excludeItem.getVariationId())) {

                                // check for exclude variations in variants list
                                for (int i = 0; i < itemList.size(); i++) {
                                    // check exclude list group id in variation list
                                    if (itemList.get(i).getGroup_id().equals(inner.getGroupId())) {
                                        for (int j = 0; j < itemList.get(i).getVariations().size(); j++) {

                                            // check exclude list variant id in variation list
                                            if (itemList.get(i).getVariations().get(j).getId().equals(inner.getVariationId())) {
                                                itemList.get(i).getVariations().get(j).setDisable(true);
                                                itemList.get(i).getVariations().get(j).setSeleceted(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    updateTextString();
                }
            }.start();

        }
        itemList.get(parentPosition).getVariations().get(childPosition).setSeleceted(true);
        groupsAdapter.notifyData(itemList);
        updateTextString();
    }

    @Override
    public void onUpdatePreviousData(int parentPosition, int childPosition) {
        itemList = originalList;
        Variation variation = itemList.get(parentPosition).getVariations().get(childPosition);
        variation.setSeleceted(false);

        itemList.get(parentPosition).getVariations().set(childPosition, variation);

        Log.e(TAG, "onUpdatePreviousData: groupId: " + itemList.get(parentPosition).getGroup_id()
                + ", variationId: " + variation.getId()
                + ", name: " + variation.getName()
                + ", isSelected: " + variation.isSeleceted());
        updateTextString();
    }

    private void updateTextString() {
        StringBuilder text = new StringBuilder();
        for (VariantGroup group : itemList) {
            for (Variation variation : group.getVariations()) {
                if (variation.isSeleceted())
                    text.append(group.getName()).append(": ").append(variation.getName()).append("\n");
            }
        }
        binding.ahTextSelected.setText(text.toString());
    }

}
