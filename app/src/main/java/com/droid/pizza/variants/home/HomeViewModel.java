package com.droid.pizza.variants.home;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.util.Log;

import com.droid.pizza.variants.R;
import com.droid.pizza.variants.infra.api.ApiService;
import com.droid.pizza.variants.infra.api.model.ExcludeList;
import com.droid.pizza.variants.infra.api.model.VariantGroup;
import com.droid.pizza.variants.infra.api.model.VariantsResponse;
import com.droid.pizza.variants.infra.livedata.SingleLiveEvent;
import com.droid.pizza.variants.infra.livedata.SnackbarMessage;
import com.droid.pizza.variants.infra.livedata.SnackbarMessageString;
import com.droid.pizza.variants.infra.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.droid.pizza.variants.infra.viewmodel.ApiStatus.HIDE_PROGRESS;
import static com.droid.pizza.variants.infra.viewmodel.ApiStatus.SHOW_PROGRESS;

public class HomeViewModel extends AndroidViewModel {
    private static final String TAG = "HomeViewModel";

    final SnackbarMessage snackbarMessage = new SnackbarMessage();
    private static SnackbarMessageString snackbarMessageString = new SnackbarMessageString();
    final SingleLiveEvent<String> apiStatus = new SingleLiveEvent<>();
    final SingleLiveEvent<ArrayList<VariantGroup>> variantGroupSingleLiveEvent = new SingleLiveEvent<>();
    final SingleLiveEvent<List<List<ExcludeList>>> excludeListSingleLiveEvent = new SingleLiveEvent<>();

    public final DataModel dataModel = new DataModel();
    public final EventHandler eventHandler = new EventHandler(this);

    private ApiService apiService;

    @Inject
    public HomeViewModel(Application application,
                         ApiService apiService) {
        super(application);
        this.apiService = apiService;
    }

    void apiCallToFetchVariants() {
        if (ConnectionDetector.isConnectingToInternet(getApplication())) {
            apiStatus.postValue(SHOW_PROGRESS);

            apiService.getVariants()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(new SingleObserver<VariantsResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(VariantsResponse response) {
                            Log.e(TAG, "onSuccess: ");
                            if (response != null) {
                                if (response.getData().getVariantGroups() != null &&
                                        response.getData().getVariantGroups().size() > 0) {
                                    for (VariantGroup group : response.getData().getVariantGroups()) {
                                        Log.e(TAG, "Name: " + group.getName());
                                    }
                                    variantGroupSingleLiveEvent.postValue(response.getData().getVariantGroups());
                                } else {
                                    snackbarMessage.postValue(R.string.no_data);
                                }

                                if (response.getData().getExcludeList() != null &&
                                        response.getData().getExcludeList().size() > 0) {
                                    excludeListSingleLiveEvent.postValue(response.getData().getExcludeList());
                                }

                                apiStatus.postValue(HIDE_PROGRESS);
                            } else {
                                apiStatus.postValue(HIDE_PROGRESS);
                                snackbarMessage.postValue(R.string.msg_general_error);
                            }

                            apiStatus.postValue(HIDE_PROGRESS);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "onError: " + e.getLocalizedMessage());
                            apiStatus.postValue(HIDE_PROGRESS);
                            snackbarMessageString.postValue(e.getLocalizedMessage());
                        }
                    });
        } else snackbarMessage.postValue(R.string.msg_alert_no_internet);
    }

    /**
     * DataModel
     */
    public static class DataModel {
        //no-op
    }

    /**
     * EventHandler
     */
    public static class EventHandler {
        private final HomeViewModel viewModel;

        public EventHandler(HomeViewModel viewModel) {
            this.viewModel = viewModel;
        }
    }
}
