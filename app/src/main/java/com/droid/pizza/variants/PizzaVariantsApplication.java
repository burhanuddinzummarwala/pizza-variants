package com.droid.pizza.variants;

import android.app.Application;

import com.droid.pizza.variants.infra.di.AppComponent;
import com.droid.pizza.variants.infra.di.AppModule;
import com.droid.pizza.variants.infra.di.DaggerAppComponent;
import com.droid.pizza.variants.infra.di.NetworkModule;

public class PizzaVariantsApplication extends Application {
    private static final String TAG = "PizzaVariantsApplicatio";
    private static PizzaVariantsApplication instance;
    private AppComponent appComponent;

    public static PizzaVariantsApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        setupDependencyInjection();
    }

    private void setupDependencyInjection() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
