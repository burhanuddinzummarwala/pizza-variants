package com.droid.pizza.variants.infra.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class VariantsResponse implements Cloneable {
    @SerializedName("variants")
    @Expose
    private Variants data;

    public Variants getData() {
        return data;
    }

    public void setData(Variants data) {
        this.data = data;
    }

    public class Variants implements Cloneable{
        @SerializedName("exclude_list")
        @Expose
        private List<List<ExcludeList>> excludeList = null;
        @SerializedName("variant_groups")
        @Expose
        private ArrayList<VariantGroup> variantGroups = null;

        public ArrayList<VariantGroup> getVariantGroups() {
            return variantGroups;
        }

        public void setVariantGroups(ArrayList<VariantGroup> variantGroups) {
            this.variantGroups = variantGroups;
        }

        public List<List<ExcludeList>> getExcludeList() {
            return excludeList;
        }

        public void setExcludeList(List<List<ExcludeList>> excludeList) {
            this.excludeList = excludeList;
        }
    }
}
