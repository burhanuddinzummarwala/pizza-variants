package com.droid.pizza.variants.infra.api;

import com.droid.pizza.variants.infra.api.model.VariantsResponse;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface ApiService {
    @GET("/b/5d70aad4fc5937640ce3820d")
    Single<VariantsResponse> getVariants();
}
