package com.droid.pizza.variants.infra.viewmodel;

public class ApiStatus {
    private ApiStatus() {
        // no-op
    }

    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILURE";
    public static final String SHOW_PROGRESS = "SHOW_PROGRESS";
    public static final String HIDE_PROGRESS = "HIDE_PROGRESS";
}
