package com.droid.pizza.variants.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.droid.pizza.variants.databinding.ItemGroupBinding;
import com.droid.pizza.variants.infra.api.model.VariantGroup;
import com.droid.pizza.variants.infra.interfaces.OnGenericRvItemClickListener;

import java.util.List;

public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.GroupViewHolder> {
    private static final String TAG = "GroupsAdapter";
    private Context mContext;
    private List<VariantGroup> itemList;
    private OnGenericRvItemClickListener listener;

    GroupsAdapter(List<VariantGroup> itemList, OnGenericRvItemClickListener listener) {
        this.itemList = itemList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        return GroupViewHolder.create(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHolder groupViewHolder, int position) {
        VariantGroup group = itemList.get(position);
        groupViewHolder.bind(group);
        VariationsAdapter variationsAdapter = new VariationsAdapter(position, group.getVariations(), listener);
        groupViewHolder.binding.igRvVariation.setAdapter(variationsAdapter);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class GroupViewHolder extends RecyclerView.ViewHolder {
        ItemGroupBinding binding;

        GroupViewHolder(ItemGroupBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        static GroupViewHolder create(ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            ItemGroupBinding binding = ItemGroupBinding.inflate(layoutInflater, parent, false);
            return new GroupViewHolder(binding);
        }

        void bind(VariantGroup item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }
    }

    void notifyData(List<VariantGroup> items) {
        this.itemList = items;
        notifyDataSetChanged();
    }

}
