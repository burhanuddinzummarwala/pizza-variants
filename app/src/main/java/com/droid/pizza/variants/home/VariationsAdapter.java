package com.droid.pizza.variants.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.droid.pizza.variants.R;
import com.droid.pizza.variants.databinding.ItemVariationBinding;
import com.droid.pizza.variants.infra.api.model.Variation;
import com.droid.pizza.variants.infra.interfaces.OnGenericRvItemClickListener;

import java.util.List;

public class VariationsAdapter extends RecyclerView.Adapter<VariationsAdapter.VariationViewHolder> {
    private static final String TAG = "GroupsAdapter";
    private Context mContext;
    private List<Variation> itemList;
    private OnGenericRvItemClickListener listener;
    private int parentPosition;
    private int selectedPosition = -1;

    public VariationsAdapter(int parentPosition, List<Variation> itemList, OnGenericRvItemClickListener listener) {
        this.itemList = itemList;
        this.listener = listener;
        this.parentPosition = parentPosition;
    }

    @NonNull
    @Override
    public VariationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        return VariationViewHolder.create(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull VariationViewHolder groupViewHolder, int position) {
        Variation variation = itemList.get(position);
        groupViewHolder.bind(variation);


        if (variation.isDisable())
            groupViewHolder.binding.ivRelativeVariation.setBackground(ContextCompat.getDrawable(mContext, R.drawable.rect_border_disable));
        else
            groupViewHolder.binding.ivRelativeVariation.setBackground(ContextCompat.getDrawable(mContext, R.drawable.rect_border_gray));

        if (variation.isSeleceted()) {
            groupViewHolder.binding.ivSelected.setVisibility(View.VISIBLE);
        } else {
            groupViewHolder.binding.ivSelected.setVisibility(View.GONE);
        }

        groupViewHolder.itemView.setOnClickListener(v -> {
            if (!variation.isDisable()) {
                if (selectedPosition == groupViewHolder.getAdapterPosition()) {
                    int prev = selectedPosition;
                    // isSelected() false
                    listener.onUpdatePreviousData(parentPosition, prev);
                    notifyItemChanged(prev);
                    selectedPosition = -1;
                    return;
                }

                if (selectedPosition >= 0) {
                    int prev = selectedPosition;
                    // isSelected() false
                    listener.onUpdatePreviousData(parentPosition, prev);
                    notifyItemChanged(prev);
                }

                selectedPosition = groupViewHolder.getAdapterPosition();
                notifyItemChanged(selectedPosition);

                // isSelected() true
                listener.onRvItemClick(parentPosition, selectedPosition, variation);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class VariationViewHolder extends RecyclerView.ViewHolder {
        ItemVariationBinding binding;

        VariationViewHolder(ItemVariationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        static VariationViewHolder create(ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            ItemVariationBinding binding = ItemVariationBinding.inflate(layoutInflater, parent, false);
            return new VariationViewHolder(binding);
        }

        void bind(Variation item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }
    }

    public void notifyData(List<Variation> items) {
        this.itemList = items;
        notifyDataSetChanged();
    }
}
