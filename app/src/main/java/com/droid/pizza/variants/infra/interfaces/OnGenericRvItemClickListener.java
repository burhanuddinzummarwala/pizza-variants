package com.droid.pizza.variants.infra.interfaces;

import com.droid.pizza.variants.infra.api.model.Variation;

public interface OnGenericRvItemClickListener {
    // select clicked variation
    void onRvItemClick(int parentPosition, int childPosition, Variation variationObj);
    // deselect previous variation
    void onUpdatePreviousData(int parentPosition, int childPosition);
}
