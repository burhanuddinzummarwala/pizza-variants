package com.droid.pizza.variants.infra.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Variation {
    @SerializedName("inStock")
    @Expose
    private Integer inStock;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("default")
    @Expose
    private Integer _default;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("isVeg")
    @Expose
    private Integer isVeg;
    private boolean isSeleceted = false;
    private boolean isDisable = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSeleceted() {
        return isSeleceted;
    }

    public void setSeleceted(boolean seleceted) {
        isSeleceted = seleceted;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public Integer get_default() {
        return _default;
    }

    public void set_default(Integer _default) {
        this._default = _default;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public boolean isDisable() {
        return isDisable;
    }

    public void setDisable(boolean disable) {
        isDisable = disable;
    }
}
