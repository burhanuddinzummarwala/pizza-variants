package com.droid.pizza.variants.infra.di;

import android.app.Application;
import android.content.Context;

import com.droid.pizza.variants.PizzaVariantsApplication;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private PizzaVariantsApplication application;

    public AppModule(PizzaVariantsApplication application) {
        this.application = application;
    }

    @Provides
    public Context provideContext() {
        return application;
    }

    @Provides
    public Application provideApplication() {
        return application;
    }
}
