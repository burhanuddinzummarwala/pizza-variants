package com.droid.pizza.variants.infra.utils;

import android.app.Dialog;
import android.content.Context;

import com.droid.pizza.variants.R;

import java.util.Objects;

public class Utils {
    private static Dialog progressDialog;

    public static void showProgress(Context context) {
        stopProgress();
        progressDialog = new Dialog(context);
        progressDialog.setContentView(R.layout.custom_dialog);
        progressDialog.setCancelable(false);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.show();
    }

    public static void stopProgress() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
