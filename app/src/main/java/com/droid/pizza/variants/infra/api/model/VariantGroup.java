package com.droid.pizza.variants.infra.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VariantGroup {
    @SerializedName("group_id")
    @Expose
    private String group_id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("variations")
    @Expose
    private List<Variation> variations;

    private boolean isSeleceted = false;

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Variation> getVariations() {
        return variations;
    }

    public void setVariations(List<Variation> variations) {
        this.variations = variations;
    }

    public boolean isSeleceted() {
        return isSeleceted;
    }

    public void setSeleceted(boolean seleceted) {
        isSeleceted = seleceted;
    }
}
